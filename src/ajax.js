const axios = require("axios");

export const wp_mytodo_task = (task) => {
   return axios
  .post("http://todo.test/wp-json/wl/v1/addTasks",{
   task
  })
  
};

export const wp_mytodo_completedTask = (bool, id) => {
 return axios
  .post("http://todo.test/wp-json/wl/v1/addCompletedTask",{
    isCompleted: bool,
     id
  }).then(res => console.log(res))
};

export const wp_mytodo_fetch_task = async () => {
  let data = await axios
    .get("http://todo.test/wp-json/wl/v1/tasks")
    
    return data;

};

export const wp_mytodo_fetch_allUser = async () => {
  let data = await axios
    .get("http://todo.test/wp-json/wl/v1/users")
    
    return data;

};

export const wp_mytodo_delete_task = (id) => {
 return axios
  .post("http://todo.test/wp-json/wl/v1/deleteTasks",{
     id
  }).then(res => console.log(res))
};

export const wp_mytodo_edit_task = (taskName, id) => {
  return axios
  .post("http://todo.test/wp-json/wl/v1/editTask",{
    taskName,
    id
  })
};

export const wp_AssignUserTask = (assignerID, id) => {
  return axios
  .post("http://todo.test/wp-json/wl/v1/AssignUserTask",{
    assignerID,
    id
  }).then(res => console.log(res))
};
