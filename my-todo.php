<?php
 /**
 * Plugin Name: My Todo
 * Description: Vue-App in WordPress.
 */

function func_load_vuescripts() {
    wp_register_script('wpvue_vuejs1', plugin_dir_url( __FILE__ ).'dist/js/chunk-vendors.js', true);
    wp_register_script('wpvue_vuejs2', plugin_dir_url( __FILE__ ).'dist/js/app.js', true);
    wp_register_style('wpvue_vuecss1', plugin_dir_url( __FILE__ ).'dist/css/chunk-vendors.css', true);
    wp_register_style('wpvue_vuecss2', plugin_dir_url( __FILE__ ).'dist/css/app.css', true);
    wp_register_script( "my_todo",  plugin_dir_url( __FILE__ ).'src/ajax.js', array('jquery') );

}
                                                                         
add_action('admin_enqueue_scripts', 'func_load_vuescripts');

add_action('admin_menu', 'vueMenuSetup');
 
function vueMenuSetup()
{
    add_menu_page(
        'My-Todo',
        'My-Todo',
        'manage_options',
        'my-todo',
        'func_wp_vue',
        'dashicons-clipboard',
        30
    );
}


function func_wp_vue(){
    wp_enqueue_style('wpvue_vuecss1');
    wp_enqueue_style('wpvue_vuecss2');
    wp_enqueue_script('wpvue_vuejs1');
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script('wpvue_vuejs2');
    wp_enqueue_script( 'my_todo' );
    

    $str= "<div id='app'>"
          ."Message from Vue:  "
          ."</div>";
    echo $str;
}

// creatating table for my-todo plugin

function myTodoTaskTable(){

$table_name = $wpdb->prefix . "todotasklist"; 
global $wpdb;

$charset_collate = $wpdb->get_charset_collate();

$sql = "CREATE TABLE $table_name (
  id mediumint(9) NOT NULL AUTO_INCREMENT,
  creatorID mediumint(9) NOT NULL,
  assignerID mediumint(9) NOT NULL,
  taskName text NOT NULL,
  userEmail text NOT NULL,
  assignTaskTime text NOT NULL,
  completedTaskTime text NOT NULL,
  completedTask BOOLEAN NOT NULL,
  PRIMARY KEY  (id)
) $charset_collate;";

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
dbDelta( $sql );
}


register_activation_hook(__FILE__, 'myTodoTaskTable');


//insert task in database

function addTask($data){
    global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    
    $result= get_current_user_id();
    $user= get_currentuserinfo();
    $time = current_time( 'mysql' );
    $test = substr($time, 11);
    $currentTime =  date("g:i a", strtotime($test));
   
    if(isset($data['task'])){

        $wpdb->insert( 
            'todotasklist', 
            array( 
                'taskName' => $data['task'],
                'completedTask' => false ,
                'creatorID' =>$result,
                'assignerID' => 0,
                'userEmail' => $user->user_email,
                'assignTaskTime' => $currentTime
            ), 
            array( 
                '%s', 
            ) 
        );

        return $result;
    };
    wp_die();

};

add_action('rest_api_init',  function(){

    register_rest_route('wl/v1', 'addTasks', [
        'methods' => 'POST',
        'callback' => 'addTask'
    ]);
});

//insert Completed task in database

function addCompletedTask($data){
    global $wpdb;
    $time = current_time( 'mysql' );
    $test = substr($time, 11);
    $currentTime =  date("g:i a", strtotime($test));
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $wpdb->update( 'todotasklist', array( 
        'completedTask' =>  $data['isCompleted'],
        'completedTaskTime' => $currentTime
    ), 
    array( 'id' => $data['id'] ),
    array( 
        '%s',
    ),
    array( 
        '%d',
        ) );
    return $data['isCompleted'];
    wp_die();
};

add_action('rest_api_init',  function(){

    register_rest_route('wl/v1', 'addCompletedTask', [
        'methods' => 'POST',
        'callback' => 'addCompletedTask'
    ]);
});


//get data from database and genereate shortcode

function myTodoListshortcode() {
  
    global $wpdb; 

    wp_enqueue_style( 'shortCode-Style', plugin_dir_url( __FILE__ ).'shortCode-Style.css' );
    $superAdmin= get_super_admins();
    $currentUserID = get_current_user_id();
   
    $result = $wpdb->get_results ( "SELECT * FROM `todotasklist`" ); 

    if($result == null){
        $content = "<h3 class='title'>No Task Assigned Yet</h3>"; 
    } else if($currentUserID == 1) {
    $content = "<h3 class='title'>Task Assigned By You</h3>"; 
    } else {
    $content = "<h3 class='title'>Task Assigned By ".$superAdmin[0]."</h3>"; 

    };

    $content .= "<ol>"; 
    foreach ($result as $print) { 
        if($print->creatorID== $currentUserID ){
            $content .= "<li>"; 
            $content .= "<span>".$print->taskName."</span>";
            $content .= "<p class='taskTime'>Task assigned at ".$print->assignTaskTime."</p>";
            $content .= "</li>";
        }
        if($print->completedTask == 1)
        {
        $content .= "<li class='completedTask'>"; 
        $content .= "<span>".$print->taskName."</span>";
        $content .= "<p class='taskTime'>Task Completed at ".$print->completedTaskTime."</p>";
        $content .= "</li>"; 
        } else if($print->assignerID == $currentUserID){
        $content .= "<li>"; 
        $content .= "<span>".$print->taskName."</span>";
        $content .= "<p class='taskTime'>Task assigned at ".$print->assignTaskTime."</p>";
        $content .= "</li>"; 
        }
    };
    $content .= "</ol>";
    
   
    return $content;

};

add_shortcode('myTodoTaskList', 'myTodoListshortcode');


//fetch all wp users from database

function fetchAllUsers() {
    global $wpdb;
    $allUsers =  get_users();
 
    $data = [];
    $i = 0;
 
    foreach($allUsers as $print){
        $data[$i]['ID'] = $print->id;
        $data[$i]['user_nicename'] = $print->user_nicename;
        $data[$i]['user_email'] = $print->user_email;
        
        $i++;
    };
 
    return json_encode($data);
 
     
}

add_action('rest_api_init',  function(){

    register_rest_route('wl/v1', 'users', [
        'methods' => 'GET',
        'callback' => 'fetchAllUsers'
    ]);
});


//Delete Task form database

function deleteTask($data) {
    global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $wpdb->delete( 'todotasklist', array( 'id' => $data['id'] ), array( '%d' ) );
    return $data['id'];
    wp_die();

}

add_action('rest_api_init',  function(){

    register_rest_route('wl/v1', 'deleteTasks', [
        'methods' => 'POST',
        'callback' => 'deleteTask'
    ]);
});


//Edit Task

function editTask($data) {
    global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $wpdb->update( 'todotasklist', array( 
        'taskName' => $data['taskName'],
    ), 
    array( 'id' => $data['id'] ),
    array( 
        '%s',
    ),
    array( 
        '%d',
        ) );
    return $data['taskName'];
    wp_die();

}

add_action('rest_api_init',  function(){

    register_rest_route('wl/v1', 'editTask', [
        'methods' => 'POST',
        'callback' => 'editTask'
    ]);
});

//Assign user task

function AssignUserTask($data) {
    global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $wpdb->update( 'todotasklist', array( 
        'assignerID' => $data['assignerID'],
    ), 
    array( 'id' => $data['id'] ),
    array( 
        '%d',
    ),
    array( 
        '%d',
        ) );
    return $data['assignerID'];
    wp_die();

}

add_action('rest_api_init',  function(){

    register_rest_route('wl/v1', 'AssignUserTask', [
        'methods' => 'POST',
        'callback' => 'AssignUserTask'
    ]);
});

// Fetch Task


function fetch_task() {
    global $wpdb;
    $ID= get_current_user_id();

    $userEmail= get_currentuserinfo();
    $result = $wpdb->get_results ( "SELECT * FROM `todotasklist`" ); 

   $data = [];
   $i = 0;

   foreach($result as $print){
       $data[$i]['ID'] = $print->id;
       $data[$i]['creatorID'] = $print->creatorID;
       $data[$i]['assignerID'] = $print->assignerID;
       $data[$i]['taskName'] = $print->taskName;
       $data[$i]['userEmail'] = $print->userEmail;
       $data[$i]['completedTask'] = $print->completedTask;
       $data[$i]['completedTaskTime'] = $print->assignTaskTime;
       
       $i++;
   };

   return json_encode($data);

 
};


add_action('rest_api_init',  function(){

    register_rest_route('wl/v1', 'tasks', [
        'methods' => 'GET',
        'callback' => 'fetch_task'
    ]);
});


?>